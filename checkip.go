package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
)

type HttpHandler struct{}

func (h HttpHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	remoteIP := strings.Split(req.RemoteAddr, ":")[0]
	log.Printf("Request from %s\n", remoteIP)
	data := []byte(fmt.Sprintf("<html><head><title>Current IP Check</title></head><body>Current IP Address: %s</body></html>", remoteIP))
	res.Write(data)
}

func main() {
	handler := HttpHandler{}
	http.ListenAndServe(":9000", handler)
}
